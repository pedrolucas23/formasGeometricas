#include <iostream>
#include "circulo.hpp"

Circulo::Circulo() {
	setLados(0);
	setLargura(15);
	setAltura(15);
}
Circulo::Circulo(float altura) {
	setLados(0);
	setLargura(altura);
	setAltura(altura);
}

float Circulo::calculaArea() {
	setArea(((getAltura() / 2.0f) * (getAltura() / 2.0f)) * 3.14f);
	return getArea();
}

float Circulo::calculaPerimetro() {
	setPerimetro(2 * (getLargura() / 2.0f) * 3.14);
	return getPerimetro();
}
