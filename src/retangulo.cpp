#include <iostream>
#include "retangulo.hpp"

Retangulo::Retangulo() {
	setLados(4);
	setLargura(10);
	setAltura(15);
}
Retangulo::Retangulo(float largura, float altura) {
	setLados(4);
	setLargura(largura);
	setAltura(altura);
}

float Retangulo::calculaArea() {
	setArea(getLargura() * getAltura());
	return getArea();
}

float Retangulo::calculaPerimetro() {
	setPerimetro((getLargura() * 2) + (getAltura() * 2));
	return getPerimetro();
}


