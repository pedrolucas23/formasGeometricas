#include <iostream>
#include "quadrado.hpp"
#include "triangulo.hpp"
#include "circulo.hpp"
#include "retangulo.hpp"

using namespace std;

int main() {

	Quadrado * forma1 = new Quadrado(20.0);
	Triangulo * forma2 = new Triangulo(20, 30);
	Circulo * forma3 = new Circulo(5);
	Retangulo * forma4 = new Retangulo(15, 25);

	forma1->calculaArea();
	forma1->calculaPerimetro();

	forma2->calculaArea();
	forma2->calculaPerimetro();

	forma3->calculaArea();
	forma3->calculaPerimetro();
	
	forma4->calculaArea();
	forma4->calculaPerimetro();

	cout << "Área do Quadrado: " << forma1->getArea() << endl;
	cout << "Perímetro do Quadrado: " << forma1->getPerimetro() << endl;

	cout << "Área do Triângulo: " << forma2->getArea() << endl;
	cout << "Perímetro do Triângulo: " << forma2->getPerimetro() << endl;

	cout << "Área do Círculo: " << forma3->getArea() << endl;
	cout << "Perímetro do Círculo: " << forma3->getPerimetro() << endl;
	
	cout << "Área do Retângulo: " << forma4->getArea() << endl;
	cout << "Perímetro do Retângulo: " << forma4->getPerimetro() << endl;

	delete(forma1);
	delete(forma2);
	delete(forma3);
	delete(forma4);
}





