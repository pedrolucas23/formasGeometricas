#ifndef RETANGULO_H
#define RETANGULO_H

#include "formageometrica.hpp"

class Retangulo : public FormaGeometrica {

	public:
		Retangulo();
		Retangulo(float largura, float altura);
		
		float calculaArea();
		float calculaPerimetro();
};
#endif
