#ifndef CIRCULO_H
#define CIRCULO_H

#include "formageometrica.hpp"

class Circulo : public FormaGeometrica {

	public:
		Circulo();
		Circulo(float altura);

		float calculaArea();
		float calculaPerimetro();
};
#endif
